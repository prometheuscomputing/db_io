This file prevents git from removing an initially empty directory.
Once you have placed some content in this directory, this placeholder can be removed.