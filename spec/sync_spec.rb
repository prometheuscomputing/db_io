require 'spec_helper'
require 'sequel'
require 'sqlite3'

describe "table syncing functionality" do
  before(:each) do
    $source = Sequel.sqlite
    $target = Sequel.sqlite

    # Create source schemas
    $source.create_table :people do
      primary_key :id
      String :name
      Fixnum :ss_number
    end
    $source.create_table :dogs do
      primary_key :id
      String :name
    end
    $source.create_table :wombats do
      primary_key :id
      String :name
    end

    # Populate source data
    $source[:people].insert(:name => 'Bob', :ss_number => 123456789)
    $source[:people].insert(:name => 'Sue', :ss_number => 987654321)
    $source[:dogs].insert(:name => 'Fido')
    $source[:dogs].insert(:name => 'Spot')
    $source[:wombats].insert(:name => 'Batty')
    $source[:wombats].insert(:name => 'Wombles')
    
    
    # Create target schemas
    $target.create_table :people do
      primary_key :id
      String :name
      # Missing ss_number
    end
    $target.create_table :dogs do
      primary_key :id
      String :name
      # Additional :bark column
      String :bark
    end
    # No wombats table
    
    # Populate target data
    $target[:people].insert(:name => 'Bob')
    $target[:people].insert(:name => 'Billy')
    $target[:dogs].insert(:name => 'Fido', :bark => 'Woof')
    $target[:dogs].insert(:name => 'Rover', :bark => 'Arf')
  end
  
  context "when using sync-type 'overwrite' on the specified target table(s) (without overwrite-schema option)" do
    context "without specifying any tables" do
      it "should sync all tables that exist in both databases" do
        options = {:sync_type => 'overwrite', :verbose => false} # No tables option specified means 'all tables'
        $target.sync($source, options)
        # people table: schema unchanged with source data replacing target data
        people = $target[:people].all
        people.length.should == 2
        people.first.keys.should =~ [:id, :name]
        people[0][:name].should == 'Bob'
        people[1][:name].should == 'Sue'
        # dogs table: schema unchanged with source data replacing target data
        dogs = $target[:dogs].all
        dogs.length.should == 2
        dogs.first.keys.should =~ [:id, :name, :bark]
        dogs[0][:name].should == 'Fido'
        dogs[0][:bark].should be_nil
        dogs[1][:name].should == 'Spot'
        dogs[1][:bark].should be_nil
        # wombats table: does not exist in target db
        $target.tables.should_not include(:wombats)
      end
    end
    
    context "when specifying the tables to sync" do
      it "should sync only the specified tables that exist in both databases" do
        options = {:sync_type => 'overwrite', :tables => ['people', 'wombats'], :verbose => false}
        $target.sync($source, options)
        # people table: schema unchanged with source data replacing target data
        people = $target[:people].all
        people.length.should == 2
        people.first.keys.should =~ [:id, :name]
        people[0][:name].should == 'Bob'
        people[1][:name].should == 'Sue'
        # dogs table: schema and data unchanged
        dogs = $target[:dogs].all
        dogs.length.should == 2
        dogs.first.keys.should =~ [:id, :name, :bark]
        dogs[0][:name].should == 'Fido'
        dogs[0][:bark].should == 'Woof'
        dogs[1][:name].should == 'Rover'
        dogs[1][:bark].should == 'Arf'
        # wombats table: does not exist in target db
        $target.tables.should_not include(:wombats)
      end
    end
  end # sync-type 'overwrite' without overwrite-schema
  
  context "when using sync-type 'overwrite' on the specified target table(s) (with overwrite-schema option)" do
    context "without specifying any tables" do
      it "should sync all tables that exist in the source database overwriting target schemas and data" do
        options = {:sync_type => 'overwrite', :overwrite_schema => true, :verbose => false} # No tables option specified means 'all tables'
        $target.sync($source, options)
        # people table: schema modified with source data replacing target data
        people = $target[:people].all
        people.length.should == 2
        people.first.keys.should =~ [:id, :name, :ss_number]
        people[0][:name].should == 'Bob'
        people[0][:ss_number].should == 123456789
        people[1][:name].should == 'Sue'
        people[1][:ss_number].should == 987654321
        # dogs table: schema modified with source data replacing target data
        dogs = $target[:dogs].all
        dogs.length.should == 2
        dogs.first.keys.should =~ [:id, :name]
        dogs[0][:name].should == 'Fido'
        dogs[1][:name].should == 'Spot'
        # wombats table: schema created and populated with source data
        wombats = $target[:wombats].all
        wombats.length.should == 2
        wombats.first.keys.should =~ [:id, :name]
        wombats[0][:name].should == 'Batty'
        wombats[1][:name].should == 'Wombles'
      end
    end
    
    context "when specifying the tables to sync" do
      it "should sync the specified tables that exist in the source database overwriting target schemas and data" do
        options = {:sync_type => 'overwrite', :overwrite_schema => true, :tables => ['people', 'wombats'], :verbose => false}
        $target.sync($source, options)
        # people table: schema modified with source data replacing target data
        people = $target[:people].all
        people.length.should == 2
        people.first.keys.should =~ [:id, :name, :ss_number]
        people[0][:name].should == 'Bob'
        people[0][:ss_number].should == 123456789
        people[1][:name].should == 'Sue'
        people[1][:ss_number].should == 987654321
        # dogs table: schema and data unchanged
        dogs = $target[:dogs].all
        dogs.length.should == 2
        dogs.first.keys.should =~ [:id, :name, :bark]
        dogs[0][:name].should == 'Fido'
        dogs[0][:bark].should == 'Woof'
        dogs[1][:name].should == 'Rover'
        dogs[1][:bark].should == 'Arf'
        # wombats table: schema created and populated with source data
        wombats = $target[:wombats].all
        wombats.length.should == 2
        wombats.first.keys.should =~ [:id, :name]
        wombats[0][:name].should == 'Batty'
        wombats[1][:name].should == 'Wombles'
      end
    end
  end # sync-type 'overwrite' without overwrite-schema
end
