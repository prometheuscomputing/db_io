require 'sequel'
require 'db_io/schema'
require 'db_io/csv'
require 'db_io/yaml'
require 'db_io/json'
require 'db_io/io'
require 'db_io/sync'
require 'db_io/meta_info'
# We do not require 'db_io/general_tools' or 'db_io/enum_migrate' because they are not always needed.

=begin

==================================================================
How to load code

Some of the bin files use: require './setup_local_api'
Some of the examples use:  require 'PROJECTNAME_generated' followed by SomeModuleName_Generated.load_ruby_api

Art does not know if the above methods work:
* setup_local_api does not seem to exist in any of the projects I have cached
* require 'PROJECTNAME_generated' is part of PROJECTNAME_irb (which loads the default database),
  but loads a lot of additional code.

The two schema export examples shown below worked on 1/19/19,
and they do not require any domain code.  It's nice to rely
only on Sequel because that way you can backup/restore/get-schema
any database, even those not constructed by our code.

A minimial test environment is:
    gem install sequel sqlite3
    cd db_io/lib
    irb
    $LOAD_PATH.unshift('.')
    require 'db_io'


==================================================================
Example - Generate a single schema file from the database.
The tables and columns are stable, so you can compare different versions.

irb
require 'db_io'
$DEFAULT_IO = DB_IO::JSON.new
# You don't need the next two lines if you used PROJECTNAME_irb instead of irb, because PROJECTNAME_irb automatically connects to a default database for that project.
   url = DB_IO.setup('../_temp/SQL.db')
   DB =  Sequel.connect(url) 
DB.export_single_schema('../_temp/Schema.json')  # YAML or JSON depending on $DEFAULT_IO

==================================================================
Example - Generate one schema file per table from the database.

irb
require 'db_io'
$DEFAULT_IO = DB_IO::JSON.new # Used by DB_IO::Schema
$BACKUP_IOs = [DB_IO::Schema.new]
# You don't need the next two lines if you used PROJECTNAME_irb instead of irb, because PROJECTNAME_irb automatically connects to a default database for that project.
   url = DB_IO.setup('../_temp/SQL.db')
   DB =  Sequel.connect(url) 
DB.backup


==================================================================
Example - back up uceproposedpev_generated database to CSV and JSON files:

irb
require 'db_io'
require 'uceproposedpev_generated'
UceProposedPev_Generated.load_ruby_api
DB.backup # Does not take argument. Goes into directory like '${Prometheus}/FLAT_TEXT_DB_BACKUPS/2011-12-17'

Another version. This one does not rely on generated code:
irb
require 'db_io'
$DEFAULT_IO = DB_IO::JSON.new # Used by DB_IO::Schema
$BACKUP_IOs = [DB_IO::CSV.new, DB_IO::JSON.new]  # [DB_IO::Schema.new, DB_IO::CSV.new, DB_IO::JSON.new, DB_IO::YAML.new]
# You don't need the next two lines if you used PROJECTNAME_irb instead of irb, because PROJECTNAME_irb automatically connects to a default database for that project.
   url = DB_IO.setup('../../Apps_Web/_temp/SQL.db')
   DB =  Sequel.connect(url) 
DB.backup


==================================================================
Example - Restore uceproposedpev_generated database from YAML files:

irb
require 'db_io'
# Wipe out the current database file - a new empty one is created by load_ruby_api
data_file = File.join(ENV['PROMETHEUS'], 'data/uceproposedpev_generated')
rm_rf(data_file) if File.exist?(data_file)
require 'uceproposedpev_generated'
UceProposedPev_Generated.load_ruby_api
# Defaults to YAML import. Can change by $DEFAULT_IO = DB_IO::JSON.new
# Can specify a path, default is like '${Prometheus}/FLAT_TEXT_DB_BACKUPS/2011-12-17' (looks inside JSON or YAML directory)
DB.import_tables 

Another version. This one does not rely on generated code:
irb
require 'db_io'
$DEFAULT_IO = DB_IO::JSON.new
# You don't need the next two lines if you used PROJECTNAME_irb instead of irb, because PROJECTNAME_irb automatically connects to a default database for that project.
   url = DB_IO.setup('../_temp/SQL.db')
   DB =  Sequel.connect(url) 
DB.import_tables '/home/vagrant/Projects/MODIFIED_FLAT_FILES' # Will look inside for JSON directory


==================================================================
Example - Copy enumeration values from src database (in old format) to dst database (in new format)

irb
require 'db_io'
require 'db_io/enum_migrate'
Enumeration_Mapping = {
    [:cognitive_bias, :obstacles_cognitive_bias] => {
        :category => :obstacles_bias_categories},
    [:obstacles, :obstacles_obstacles] => {
        :type => :obstacles_types,
        :primary_category => :obstacles_categories,
        :secondary_category => :obstacles_categories,
        :primary_emotion => :obstacles_emotions,
        :secondary_emotion => :obstacles_emotions,
        :primary_perfectionistic_trait => :obstacles_trait_of_perfectionisms,
        :secondary_perfectionistic_trait => :obstacles_trait_of_perfectionisms,
        :primary_perfectionistic_tool => :obstacles_tool_of_perfectionisms,
        :secondary_perfectionistic_tool => :obstacles_tool_of_perfectionisms}
    }  
src_url = DB_IO.setup('../_temp/SQL.db')
dst_url = DB_IO.setup '/home/vagrant/Prometheus/data/obstacles_generated/SQL.db'
DB_IO.migrate_enumerations(src_url, dst_url, Enumeration_Mapping)    

==================================================================
Example - Set or change markup language everywhere in the database

irb
require 'db_io'
require 'db_io/enum_migrate'
# You don't need the next two lines if you used PROJECTNAME_irb instead of irb, because PROJECTNAME_irb automatically connects to a default database for that project.
   url = DB_IO.setup('../_temp/SQL.db')
   DB =  Sequel.connect(url) 
DB_IO.set_markup_language(DB, 'Kramdown')

=end
