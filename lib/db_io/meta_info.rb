# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module DB_IO
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb

  GEM_NAME = 'db_io'
  VERSION = '1.0.0'
  AUTHORS = ['Art Griesser', 'Michael Faughn']
  EMAILS = ['michael.faughn@nist.gov']
  HOMEPAGE = 'https://gitlab.com/prometheuscomputing/db_io'
  SUMMARY = %q{Primary use is migrating DB from old schema to new schema.  Also, export/Import RDB to/from flat text files}
  DESCRIPTION = %q{Saves the content of a relational database in a CSV, YAML, or JSON file.
    Also restores the database from those files, ignoring data whose mapping is not obvious.
    This can therefore be used a quick and dirty alternative to Sequel "migrations", especially
    when the only changes to the model are additons. Operates at the Sequel level using Hashes,
    rather than Models. }

  LANGUAGE = :ruby
  LANGUAGE_VERSION = '~> 3.2'
  RUNTIME_VERSIONS = { :mri => '~> 3.2' }
  TYPE = :library

  DEPENDENCIES_RUBY = {
    'sequel' => '~> 5.2'
   }
end
