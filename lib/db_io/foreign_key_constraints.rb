# Jeremy Evans has no intention of supporting disabling of foreign keys.
# [6:58pm] artg: Is there a way in Sequel to either turn off foreign key constraints, or to empty all the tables (other than system tables, such as user permission tables)?  I refer to something along the lines of https://github.com/laravel/framework/issues/2502.  The second comment on this page contains DDL code to disable/reenable foreign keys for a half dozen different databases.
# [7:00pm] jeremyevans: artg: Sequel doesn't have an API for it, but you can pass the appropriate SQL to Database#run to do that.  Something like: DB.synchronize{DB.run "SET FOREIGN_KEY_CHECKS = 0"; ...}
# [7:00pm] artg: Thank you!
# [7:00pm] jeremyevans: artg: I don't plan on adding an API to Sequel for it, it's very dangerous and should only be used if absolutely necessary


module Sequel
  
  class Database
    # This temporarily turns off checking foreign key constraints while running the block you pass in, and then turns on the constraints again.
    # It's possible to corrupt the database this way: turning the contraints back on does not verify that the constraints are met.
    def run_without_referential_integrity(&proc)
      raise 'This database does not yet support disabling/enabling of foreign key constraints.' unless supports_disabling_referential_integrity?
      synchronize {
        run disable_referential_integrity_sql
        proc.call
        run enable_referential_integrity_sql
        }
     end
    def supports_disabling_referential_integrity?; false; end
    def supports_verifying_referential_integrity?; false; end
    def verify_referential_integrity
      raise 'This database does not yet support bulk validation of foreign key constraints.' unless supports_verifying_referential_integrity?
      synchronize { run verify_referential_integrity_sql }
    end
  end

=begin
  module Postgres::DatabaseMethods
    # For a way to disable/enable referential integrity, see http://www.postgresql.org/message-id/4EA2057F.3000002@ringerc.id.au
  end

  module MySQL::DatabaseMethods
    def supports_disabling_referential_integrity?; true; end
    def disable_referential_integrity_sql; 'SET FOREIGN_KEY_CHECKS = 0;'; end
    def enable_referential_integrity_sql; 'SET FOREIGN_KEY_CHECKS = 1;'; end
    # For a way to verify referential integrity, please see https://www.percona.com/blog/2011/11/18/eventual-consistency-in-mysql/
  end

  module Mysql2::DatabaseMethods
    def supports_disabling_referential_integrity?; true; end
    def disable_referential_integrity_sql; 'SET FOREIGN_KEY_CHECKS = 0;'; end
    def enable_referential_integrity_sql; 'SET FOREIGN_KEY_CHECKS = 1;'; end
    # For a way to verify referential integrity, please see https://www.percona.com/blog/2011/11/18/eventual-consistency-in-mysql/
  end
=end

  module SQLite
    module DatabaseMethods
      def supports_disabling_referential_integrity?; true; end
      def supports_verifying_referential_integrity?; true; end
      def disable_referential_integrity_sql; 'PRAGMA foreign_keys = OFF;'; end
      def enable_referential_integrity_sql; 'PRAGMA foreign_keys = ON;'; end
      def verify_referential_integrity_sql; 'PRAGMA foreign_key_check;'; end
    end
  end

end




