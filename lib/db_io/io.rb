
require 'date'
require 'sequel'
require 'fileutils'
include FileUtils

require 'db_io/foreign_key_constraints'
require 'db_io/string_fix'

module DB_IO
  # Loads database access code, based on the url.
  # Returns a url suitable for feeding to Sequel.connect
  # If the argument is a filepath, sqlite is presumed and a url is constructed.
  # If sqlite is used, raises an error if must_exist is true and the file is missing
  def self.setup(url_or_filepath, must_exist=true)
      url = url_or_filepath
       unless url_or_filepath.index(':')
          filepath = File.absolute_path(url_or_filepath)
          # We do not test for existence here, becuase the method might be passed a url
          jdbc = 'jruby' == RUBY_ENGINE
          url = jdbc ? "jdbc:sqlite:#{filepath}" : "sqlite://#{filepath}"
       end
       case
       when 0==url.index('sqlite') 
          require 'sqlite3'
          if must_exist
              filepath = url.sub('sqlite://', '')
              raise("Required database file is missing: #{filepath}") unless File.exist?(filepath)
          end
       when 0==url.index('jdbc:sqlite')
          require 'sqlite3'
          if must_exist
              filepath = url.sub('jdbc:sqlite:', '')
              raise("Required database file is missing: #{filepath}") unless File.exist?(filepath)
          end
       when 0==url.index('postgres'); require 'pg'
       when 0==url.index('mysql2'); require 'mysql2'
       else raise "Unknown database type: #{url.inspect}"
       end
       url
  end
end


class Array
  # Presumes the reciever is an Array of Hashes.
  # Returns a new Array made of new Hashes that have sorted keys.
  def sort_keys
    collect {|row|
      new_row = Hash.new
      keys = row.keys.sort
      keys.each {|key| new_row[key]=row[key] }
      new_row
    }
  end

  # Presumes the reciever comes from aSequelDatabase.schema(table_name):
  #    "an array with all members being arrays of length 2, the first member being the column name,
  #    and the second member being a hash of column information."
  def sort_schema
    sort {|a, b| a.first <=> b.first }
  end
end

class Sequel::Dataset

  # These globals let you define transformations on Strings. They are a work-around for a pesky coding mismatch that
  # currently happens if you paste text from some PDFs into GUIs built by the gui builder.
  $FORCE_ENCODING= nil # 'UTF-8'
  $CHANGE_ENCODING= nil # 'UTF-8'
  # According to ruby.rupaint.org:
  # "Ruby defines an encoding named'ASCII-8BIT', with an alias of 'BINARY', which does not
  # correspond to any known encoding.  It is intended to be associated with binary data,
  # such as the bytes that make up a PNG image, so has no restrictions on content.
  # One byte always corresponds with one character."
  # "ASCII-8BIT, then, effectly corresponds to the absence of an encoding, so methods
  # that expect an encoding name recognise nil as a synonym."
  # Note that the arguments to Iconv are backwards:  Iconv.new(to_encoding, from_encoding)
  $CONVERTER = nil # Iconv.new('UTF-8', 'ISO-8859-1')
  # If present, a Hash of the form { pattern => replacement_value}.  These are fed to gsub.
  $REPLACEMENTS = nil # {/\P{ASCII}/ => ''} # '\AHuman\Z' => 'Person'

  # Some problems need to be fixed in the export, rather than the import. Example: when exporting to JSON the sample ONC use case (with much pasted text), you get:
  #     db_io/lib/db_io/json.rb:16:in `encode': "\xE2" from ASCII-8BIT to UTF-8 (Encoding::UndefinedConversionError)
  # Applied to all cell values. Currently configured to get rid of windows characters.
  # FIX - would be nice to pass into this the type of data stored in this column
  # CAUTION -  THIS VERSION WILL CORRUPT BINARY FILES!
  $EXPORT_TRANSFORMER = lambda {|key, value| value.fix_for_db_export }

  # Returns an Array of hashes.
  #   How do you know if aSequelDataset.all returns an Array of Hashes, or of Model subclass instances?
  #      Can we safely presume that no model defintions are loaded?
  def export_rows
    return all unless $EXPORT_TRANSFORMER
    answer = Array.new
    all.each {|input_hash|
      # Think we can save result of "all", iterate over that, directly modify input_hash, and return that cached "all" (without modifying database). Being paranoid here.
      output_hash = Hash.new
      answer << output_hash
      input_hash.each {|key, value|
        output_hash[key]=$EXPORT_TRANSFORMER.call(key, value)
        }
      }
    answer
  end

  # Add rows to the database. Presumes the data types for columns have not changed (if necessary the CSV, YAML, or JSON file can be processed before import).
  # rows is an Array of hashes.  Ignores keys which are present in a row, but which are not in importable_columns.
  # If importable_columns is nil, it is computed (in some cases it may be better to compute importable_columns at a higher level).
  # FIX:   The table_name argument is used only for error reporting.
  #        It would be better to get the table name from the receiver, but seems to be no way to do so.
  #        To eliminate this, pass in an error_array for this specific table, instead of the general error_hash.
  #        Maybe switch sequence so that importable_colums (often nil) is last.
  # CAUTION: It appears "aSequel::Dataset <<" honors provided id values (error if you import a row with an id already in use).
  #          If it does not use the provided id values, broken associations would result.
  def import_rows(rows, table_name, importable_columns, options)
    options[:error_hash] ||= {}
    unless importable_columns
      example = rows[0]
      return unless example
      importable_columns = example.keys & columns
    end
    if importable_columns.empty?
      options[:error_hash][table_name]='No importable columns'
      return options
    end
    if rows.empty?
      options[:error_hash][table_name]='No rows'
      return options
    end
    rows.each do |row|
      begin
        row = row.select {|col, val| importable_columns.member?(col) }
        row.values.each {|value| next unless value.kind_of?(String); value.encode!($CHANGE_ENCODING) } if $CHANGE_ENCODING
        row.values.each {|value| next unless value.kind_of?(String); value.force_encoding($FORCE_ENCODING) } if $FORCE_ENCODING
        row.keys.each {|key| value=row[key]; next unless value.kind_of?(String); row[key]=$CONVERTER.iconv(value) } if $CONVERTER
        if $REPLACEMENTS
          $REPLACEMENTS.each {|pattern, replacement|
            row.values.each {|value| next unless value.kind_of?(String); value.gsub!(pattern, replacement) }
            }
        end
        self << row
      rescue => err
        (options[:error_hash][table_name] ||= Array.new) << [err.message, row, err.backtrace]
      end
    end
  end

  def blob_columns
    table_schema = db.schema(self)
    table_schema.select{|col, col_info| col_info[:type] == :blob }
  end

end

# Add high level functionality to Sequel::Database
class Sequel::Database

  $DEFAULT_IO = DB_IO::YAML.new
  $BACKUP_IOs = [DB_IO::Schema.new, DB_IO::CSV.new, DB_IO::YAML.new, DB_IO::JSON.new]
  $BACKUP_DIR = File.expand_path(File.join( (ENV['PROMETHEUS'] || '~'), 'FLAT_TEXT_DB_BACKUPS'))
  $BACKUP_DIR_REPLACE = false


  def default_directory_path; File.join($BACKUP_DIR, Date.today.to_s); end

  # Export all the tables (except those specified in ignored_tables) using all the methods specified by $BACKUP_IOs
  # specific_tables and ignored_tables must be Symbols
  def backup(specific_tables=[], ignored_tables=[], directory_path=default_directory_path)
    $BACKUP_IOs.each {|io| export_tables(directory_path, io, specific_tables, ignored_tables) }
  end

  # Syncronize this database with the given source_db
  def sync(source_db, options = {})
    options[:renamed_tables] ||= {}
    DB_IO::Sync.sync_databases(source_db, self, options)
  end


  # This method writes a schema in YAML or JASON form to a single file.
  # io must be a DB_IO::YAML or DB_IO::JSON (not a DB_IO::Schema or DB_IO::CSV)
  # This differs from backup when $BACKUP_IOs contains an instance of DB_IO::Schema - because that produces one schema file per table.
  # specific_tables and ignored_tables must be Symbols
  def export_single_schema(file_path, io=$DEFAULT_IO, specific_tables=[], ignored_tables=[])
    specific_tables = tables if specific_tables.empty?
    specific_tables = specific_tables.sort
    schema_array= Array.new
    specific_tables.each {|table_name|
        table = self[table_name]
        schema = table.db.schema(table_name)
        schema = schema.sort_schema
        schema_array << "===== Schema for table #{table_name} ====="
        schema_array << schema
      }
    io.marshall(file_path, schema_array)
  end

  # Export the entire DB to flat files (one per table) under the specified directory.
  # This derives a data path from the specified directory_path and the name of the io.
  # This deletes any existing directory at the data path if $BACKUP_DIR_REPLACE
  # specific_tables and ignored_tables must be Symbols
  def export_tables(directory_path=default_directory_path, io=$DEFAULT_IO, specific_tables=[], ignored_tables=[], sort=true)
     data_path = File.join(directory_path, io.class.name.split('::').last)
     if File.exist?(data_path)
       $BACKUP_DIR_REPLACE ? rm_rf(data_path) :  raise("Attempt to overwrite #{data_path.inspect}, not allowed by $BACKUP_DIR_REPLACE")
     end
     mkdir_p(data_path) # We know data_path does not exist
     specific_tables = tables if specific_tables.empty?
     specific_tables = specific_tables.sort if sort
     cd(data_path) {
       specific_tables.each {|table_name|
         next if  ignored_tables.member?(table_name)
         export_table(table_name, io, sort)
         }
       }
  end

  # Exports specified table to flat file with same name in current directory.
  # Overwrites any existing file with this name.
  # table_name must be a Symbol
  def export_table(table_name, io=$DEFAULT_IO, sort=true)
    table = self[table_name]
    io.export(table_name, table, sort)
  end

  
  def _import_tables(directory_path=default_directory_path, table_names=tables, io=$DEFAULT_IO, options={})
    options[:error_hash] ||= {}
    data_path = File.join(directory_path, io.class.name.split('::').last)
    raise "Unable to find directory #{data_path}" unless File.exist?(data_path)
    cd(data_path) {  table_names.each {|table_name| import_table(table_name, io, options) } }
    show_errors(options[:error_hash])
    options
  end

  # Import the flat files (one per table) in the specified directory.
  # Driven by the database schema, rather than file system. This uses import_table.
  # NOTE: since this is schema-driven
  #   * it can't be used to import a schema
  #   * it can't import a file that doesn't have a corresponding table in the database
  # See comments on import_table.
  # This should have had the signature used by _import_tables, but I'd like to keep it backwards compatable.
  def import_tables(directory_path=default_directory_path, io=$DEFAULT_IO, options = {})
    options[:error_hash] ||= {}
    data_path = File.join(directory_path, io.class.name.split('::').last)
    raise "Unable to find directory #{data_path}" unless File.exist?(data_path)
    cd(data_path) {  tables.each {|table_name| import_table(table_name, io, options) } }
    show_errors(options[:error_hash] )
    options
  end

  # Imports specified table from flat file with same name in current directory.
  # Does not clear the table.
  # Unless you are careful to avoid importing to an already populated database,
  # this should be used only on a new database that's completely empty (including adminrator login).
  # Presumes the data types for columns have not changed (if necessary the CSV, YAML, or JSON file can be processed before import).
  def import_table(table_name, io=$DEFAULT_IO, options = {})
    table = self[table_name] unless io.is_a?(DB_IO::Schema)
    io.import(table_name, table, options, self)
    options
  end

  # This is identical to import_tables, except that
  #    * it removes all table content frist  # enumerations the database first.
  #    * the history is not imported if include_history is false. That includes the change tracker tables, and the tables of deleted data.
  def restore(directory_path=default_directory_path, include_history=false, io=$DEFAULT_IO, options={})
    supports_restore!
    table_names = tables
    unless include_history
      table_names = table_names.reject {|name| /^ct_/ === name }
      table_names = table_names.reject {|name| /_deleted$/ === name }
    end
    run_without_referential_integrity {
      # We remove the tables of enumartions which are automatically populated on connection.
      # eunums = Sequel::Model.children.select{|m| m.enumeration? }
      # enums.each{|e| self[enum.table_name].delete }
      # Have not verified that this does not wipe out meta tables, such as user permissions.
      #    Need to base this on what's actually in the database, rather than models (otherwise problems when DB does not contain a new model)
      #    Sequel::Model.children.each {|model| model.delete }
      # No, can't import due to missing table
      #    drop_table *tables
      Sequel::Model.children.each {|model| model.delete if tables.member?(model.table_name) }
      # We do the import without referential integrity because
      #  * it's difficult to ensure the files are imported in the correct sequence
      #  * it's faster to import without those checks
      _import_tables(directory_path, table_names, io, options)
    }
    verify_referential_integrity
  end

  def supports_restore!
    d = supports_disabling_referential_integrity?
    v = supports_verifying_referential_integrity?
    return if d && v
    msg = "Restore currently requires temporary suspension of referential integrity, and subsequent validation of integrity."
    msg << "  This database does not support disabling of foreign key checks." unless d
    msg << "  This database does not support validation of foreign key checks." unless v
    raise msg
  end

  def show_errors(hash)
    error_hash = hash[:error_hash] || hash # attempt at backwards compatibility
    error_hash.each do |key, errors|
  	  if errors.kind_of?(Array)
  	    error = errors[0] # Ignore all but the first error since they are likely to repeat
  	    puts "\nTable: #{key.inspect} --- #{error[0]}"
  	    puts "  on row: #{error[1].inspect}"
  	    puts "  num errors on this table: #{error.length}"
  	    puts "  full trace:"
  	    puts error[2]
  	    puts
  	  else
  	    puts "\n#{key} --- #{errors}"
  	  end
    end
  end
end # class Sequel::Database
