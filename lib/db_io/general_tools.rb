
=begin

Currently includes functionality for 
  * serializing/deserializing object graphs into/from JSON/YAML files
  * analyzing the format of JSON/YAML files

=end


require 'yaml'
# requires  "gem install json_pure" or "gem install json"
require 'json'; 
require 'json/add/core' # serializes instances of Range, Regex, Exception, Time, etc.
require 'db_io/json_rails' # Before it was deprecated and copied here, this file was 'json/add/rails'

$ERROR_MESSAGE_LIMIT = 10 # Max number of lines of lower level error message that are embedded in parse error message. If 0 or less, number of lines is unlimited
$REPAIR_DIR = nil  # If non-nil writes corrected files here
$STRUCT_DIR = nil  # If non-nil, writes data structure analysis here
$WRITE_FORMAT = :yaml # One of :json, :yaml, :same  CAUTION - JSON default output results in very long lines that can cause problems for text editors

# An Array of two-arg Procs, {|file_path, contents| ... }.  Each Proc modifies the contents string it's given, and returns corrected string. Procs are applied sequentially.
$DOMAIN_SPECIFIC_REPAIRS = [] 


class Object
  def boolean?; false; end
end
class TrueClass
  def boolean?; true; end
end
class FalseClass
  def boolean?; true; end
end



# ################## Parsing ######################

# Return a String containing the contents of the specified file, with $DOMAIN_SPECIFIC_REPAIRS applied.
# If $REPAIR_DIR is non-nil, corrected files are written there (for diagnostic purposes, or for subsequent use without need to fix again)
def contents_of(file_path)
  contents = File.read(file_path)
  contents = $DOMAIN_SPECIFIC_REPAIRS.inject(contents) {|contents, proc| proc.call(file_path, contents) }
  File.open(File.join($REPAIR_DIR, File.basename(file_path)), 'w') {|f| f << contents } if $REPAIR_DIR && !$DOMAIN_SPECIFIC_REPAIRS.empty?
  contents
end

# Accepts mutliple patterns or file paths. Last argument can be a Boolean, false for non-silent (raise exception on error).  Defaults to silent=true.
# Returns:
#   * a single root or processor result if a single file path is specified
#   * an Array under all other conditions (a pattern is specified or multiple file paths are specified)
def parse_files(*patterns, &processor)
  silent = patterns.last.boolean? ? patterns.pop : true
  return _parse_files(patterns.first, silent, &processor) if 1==patterns.size
  answer = Array.new
  patterns.each {|pattern| 
    results = _parse_files(pattern, silent, &processor)
    if results.instance_of?(Array)
      results.each {|result| answer << result }
    else
      answer << results
    end
    }
  answer
end

# Identical to _parse_file, but accepts a single pattern or file path
# Returns:
#   * a single root or processor result if a file path is specified
#   * an Array if a pattern is specified.
def _parse_files(pattern, silent=true, &processor)
  return _parse_file(pattern, silent, &processor) unless pattern.index('*')
  files = Dir[pattern]
  files.sort.collect {|path| _parse_file(path, silent, &processor) }
end

# Returns a root object (unless a processor is provided, in which case it's result is returned). 
# Optional processor is a block of form {|file_path, root| ... }
# If processor is provided, it is called even if the file could not be processed (nil is passed as root).
# Error handling depends on "silent". If silent, returns nil if unable to parse.
# First argument is a file path (without wildcards)
def _parse_file(file_path, silent=true, &processor)
  root = begin
    case File.extname(file_path)
      when '.yaml'; 
        # Does not let us modify content: YAML.load_file(file_path)
        YAML.load contents_of(file_path)
      when '.json';
        JSON.parse contents_of(file_path)
      else return problem(silent, "Unknown file type: #{file_path.inspect}")
    end
  rescue => err
    message = err.message
    message = message.split("\n")[0..($ERROR_MESSAGE_LIMIT-1)].join("\n") if $ERROR_MESSAGE_LIMIT && $ERROR_MESSAGE_LIMIT > 0
    return problem(silent, "Failed to parse file: #{file_path.inspect}, error: #{message}")
  end 
  problem(silent, "No root for file: #{file_path.inspect}") unless root
  return processor.call(file_path, root) if processor
  root
end

def problem(silent, message); silent ? puts(message) : raise(message); nil end

# ################## Write File ######################

# If $WRITE_FORMAT is :same, the output format is determined by the file extension.
# If $WRITE_FORMAT is anything else, the file extension and format are determined by $WRITE_FORMAT (the extension on file_path is ignored)
def write_file(file_path, root, silent=false)
  file_path = file_path.normalize_fileType
  str = case File.extname(file_path)
    when '.yaml'; root.to_yaml
    when '.json'; root.to_json
    else return problem(silent, "Unknown file type: #{file_path.inspect}")
  end
  File.open(file_path, "w") {|file| file << str }
end

class String
  # presumes receiver represents a file path. Modifies the extension to satisfy $WRITE_FORMAT
  def normalize_fileType;
    return self if :same == $WRITE_FORMAT
    base = File.basename(self, '.*')
    dir = File.dirname(self)
    File.join(dir, "#{base}.#{$WRITE_FORMAT}")
  end
end

# ################## Analysis ######################

# Accepts mutliple patterns or file paths. Last argument can be a Boolean, false for non-silent (raise exception on error).  Defaults to silent=true.
# For each input file, creates a File_Structure that describes the structure of the file.
# If multiple files have the same structure, their paths are added to the other_file_paths collection of the first enouncountered similar File_Structure.
# Outputs unique File_Structure instances on the transcript, and, if $STRUCT_DIR is provided, also writes them to files.
# returns nil.
def analyze(*patterns)
  structs = Array.new # Includes only unique structures
  parse_files(*patterns) {|file_path, root| 
    struct = File_Structure.new(file_path, root)
    found = structs.detect {|s| s == struct }
    found ? found << struct : structs << struct
    }
  structs.each {|struct|
    path = struct.file_path
    str = struct.tree_dump
    str = str.gsub(/ id=\d+\n/, "\n") # We to not need ids here because these data structures from a tree without any back references. The ids make comparisons harder.
    puts "\n"
    puts str
    File.open(File.join($STRUCT_DIR, File.basename(path)), 'w') {|f| f << str } if $STRUCT_DIR
    }
  nil
end


def _analyzer(*data_types)
  case
    when data_types.member?('Hash'); Hash_Structure.new
    when data_types.member?('Array'); Array_Structure.new
    else nil
  end
end


class File_Structure
  include Comparable
  attr_accessor :file_path,
                :type,            # A class name
                :value_structure,  # Typically a Hash_Structure
                :other_file_paths
  def initialize(file_path, unmarshalled_data=nil)
    self.file_path=file_path
    return unless unmarshalled_data
    analyze(unmarshalled_data)
    finalize
  end
  def analyze(unmarshalled_data)
    self.type = unmarshalled_data.class.name
    self.value_structure = _analyzer(type)
    value_structure.analyze(unmarshalled_data) if value_structure
  end
  # Replace Sets by Arrays to make it easier to examine results
  def finalize
    value_structure.finalize if value_structure
  end
  def <<(other)
    (self.other_file_paths ||= Array.new) << other.file_path
  end
  def <=>(other)
    return 1 unless other && self.class == other.class
    comparison = type <=> other.type
    return comparison unless 0==comparison
    value_structure <=> other.value_structure
  end
end

class Hash_Structure
  include Comparable
  attr_accessor :key_types,     # A class name
                :keys,
                :key_structure,  # non-nil only for keys that are Hashes
                :value_types,     # A class name
                :value_structure # non-nil only for values that are Hashes
  def initialize
    self.key_types = Set.new
    self.keys = Set.new
    self.value_types = Set.new
  end
  def finalize
    self.key_types = key_types.to_a
    self.keys = keys.to_a
    self.value_types = value_types.to_a
    key_structure.finalize if key_structure
    value_structure.finalize if value_structure
  end
  def analyze(unmarshalled_data)
    return unless unmarshalled_data.kind_of?(Hash)
    unmarshalled_data.each {|key, value|
      key_types << key.class.name
      value_types << value.class.name
      keys << key if key.kind_of?(String) || key.kind_of?(Numeric)
      }
    self.key_structure = _analyzer(*key_types) unless key_structure
    self.value_structure = _analyzer(*value_types) unless value_structure
    unmarshalled_data.each {|key, value|
      key_structure.analyze(key) if key_structure
      value_structure.analyze(value) if value_structure
      }
  end
  def <=>(other)
    return 1 unless other && self.class == other.class
    comparison = key_types.sort <=> other.key_types.sort
    return comparison unless 0==comparison
    comparison = value_types.sort <=> other.value_types.sort
    return comparison unless 0==comparison
    comparison = keys.sort <=> other.keys.sort
    return comparison unless 0==comparison
    comparison = key_structure <=> other.key_structure if key_structure
    return comparison unless 0==comparison
    comparison = value_structure <=> other.value_structure if value_structure
    comparison
  end
end

class Array_Structure
  include Comparable
  attr_accessor :types,     # A class name
                :element_structure # non-nil only for values that are Hashes
  def initialize
    self.types = Set.new
  end
  def finalize
    self.types = types.to_a
    element_structure.finalize if element_structure
  end
  def analyze(unmarshalled_data)
    return unless unmarshalled_data.kind_of?(Array)
    unmarshalled_data.each {|element|
      types << element.class.name
      }
    self.element_structure = _analyzer(*types) unless element_structure
    unmarshalled_data.each {|element|
      element_structure.analyze(element) if element_structure
      }
  end
  def <=>(other)
    return 1 unless other && self.class == other.class
    comparison = types.sort <=> other.types.sort
    return comparison unless 0==comparison
    comparison = element_structure <=> other.element_structure if element_structure
    comparison
  end
end
