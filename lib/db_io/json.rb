# http://flori.github.com/json/    I used json pure:
#     gem install json_pure
# Another Ruby JSON library is http://github.com/brianmario/yajl-ruby
#require 'json/pure'
#require 'json/add/core' # 'json/add/core' serializes instances of Range, Regex, Exception, Time, etc.
#require 'json/add/rails'# 'json/add/rails' lets you serialize arbitrary objects
require 'json'
require 'json/add/core' rescue nil # 'json/add/core' serializes instances of Range, Regex, Exception, Time, etc.
# Rescue load errors here since json versions > 1.6.0 do not have json/add/rails (it is already included)
begin
  require 'json/add/rails' # 'json/add/rails' lets you serialize arbitrary objects
rescue LoadError
end

module DB_IO
  def self.pretty; @pretty||=true; end
  def self.pretty=(bool); @pretty=bool; end

class JSON
  
  def marshall(file_path, object)
    content = DB_IO.pretty ? ::JSON.pretty_generate(object) : object.to_json
    File.open(file_path, "w") {|file| file << content }
  end 

  # Writes the table to a JSON file in the current directory, as an Array of Hashes, each Hash representing one row.
  # table is a Sequel::Dataset (sequel does not have an explicit Table class).
  def export(table_name, table, sort=true)
    file_name= "#{table_name}.json"
    exported_rows = table.export_rows
    exported_rows = exported_rows.sort_keys if sort
    content = DB_IO.pretty ? ::JSON.pretty_generate(exported_rows) : exported_rows.to_json
    File.open(file_name, "w") {|file| file << content } 
  end
  
  # Import file (written by companion #export) from current directory into specified table.
  # table is a Sequel::Dataset (sequel does not have an explicit Table class).
  def import(table_name, table, options={}, target_db)
    options[:error_hash] ||= {}
    file_name= "#{table_name}.json"
     unless File.exist?(file_name)
      options[:error_hash][table_name]='Missing file'
      return options
    end
    begin
      contents = File.read(file_name)
      rows = ::JSON.parse(contents)
      # import_rows expect keys to be symbols. Yaml is using Symbols.  Maybe we should make YAML write string keys instead.
      rows = rows.collect {|row| 
        symbol_hashed = Hash.new
        row.each {|key, value| symbol_hashed[key.to_sym]=value }
        symbol_hashed
        }
      table.import_rows(rows, table_name, nil, options)
    rescue => err
      options[:error_hash][table_name]=err.message
    end
  end

end

end # module