require 'tmpdir'
require 'rainbow'
module DB_IO
  class Sync
    def self.sync_databases(source_db, target_db, options)
      defaults = {:sync_type => 'overwrite', :overwrite_schema => false, :verbose => true}
      options = defaults.merge(options)
      if options[:tables] && options[:tables].any?
        tables = options[:tables].map { |t| t.to_sym }
      else
        tables = source_db.tables
      end
      tables = tables - Array(options[:excluded_tables])
      # Ensure that tables only contains tables in both source and target databases
      tables = tables & source_db.tables

      unless options[:overwrite_schema] && options[:sync_type] == 'overwrite'
        # remove invalid tables from renamed_tables hash
        options[:renamed_tables].reject { |k,v| !(tables.include? k) || !(target_db.tables.include? v)}
        # remove tables that aren't either in both databases or are in the source database and are renamed in the target database
        tables.reject! { |t| !(target_db.tables.include? t) && !(options[:renamed_tables].keys.include? t) }
      end
      
      
      # Handle existing target_db tables according to sync_type
      case options[:sync_type]
      when 'overwrite'
        if options[:overwrite_schema]
          puts "Dropping tables: #{tables.collect{|t|t.to_s}.join(', ')}" if options[:verbose]
          tables.each {|t| target_db.drop_table(t) if target_db.table_exists?(t)}
          print "Overwriting schemas...  " if options[:verbose]
          sync_schema_export_dir = Dir.mktmpdir('sync_schema_export_')
          source_db.export_tables(sync_schema_export_dir, DB_IO::Schema.new, tables)
          cd(sync_schema_export_dir + '/Schema') {
            tables.each do |table|
              target_db.import_table(table, DB_IO::Schema.new, {})
            end
          }
          FileUtils.remove_entry sync_schema_export_dir
          puts "Done" if options[:verbose]
        else
          # THIS IS THE NORMAL / DEFAULT USAGE
          tables.each do |table|
            print "Clearing table: #{table}...  " if options[:verbose]
            target_table = options[:renamed_tables][table] || table
            target_db[target_table].delete
            puts "Done" if options[:verbose]
          end
        end
      when 'merge'
        # Do nothing
      else
        raise "Invalid SYNC_TYPE specified: #{options[:sync_type]}"
      end
      
      # Import the appropriate data from source_db
      puts "Now importing data" if options[:verbose]
      if options[:export_import]
        # Create temp dir
        sync_export_dir = Dir.mktmpdir('sync_export_')
        print "Starting YAML export...  " if options[:verbose]
        source_db.export_tables(sync_export_dir, DB_IO::YAML.new, tables)
        puts "Done"
        print "Starting YAML import...  " if options[:verbose]
        target_db.import_tables(sync_export_dir, DB_IO::YAML.new)
        puts "Done" if options[:verbose]
        # Remove temp dir
        FileUtils.remove_entry sync_export_dir
      else
        # THIS IS THE NORMAL USAGE of #sync
        tables.each do |table|
          print "Importing data into #{table}...  " if options[:verbose]
          # Select only columns in common between source and target tables
          target_table      = options[:renamed_tables][table] || table
          migrated_columns  = source_db[table].columns & target_db[target_table].columns
          column_mapping    = options.dig(:renamed_columns, table)
          migrated_columns = (migrated_columns + column_mapping.keys).uniq if column_mapping
          source_dataset = source_db[table].select(*migrated_columns)
          all_rows = source_dataset.all
          if column_mapping
            column_mapping.each { |k,v| puts Rainbow("^^Mapping^^ #{table}.#{k} to #{target_table}.#{v}").cyan }
            all_rows = all_rows.map { |row| map_row_to_renamed_columns(row, column_mapping) }
          end
          
          # Import the source data into the target
          target_db[target_table].multi_insert(all_rows)
          puts "#{all_rows.count} rows imported using columns: #{migrated_columns.map { |c|c.to_s }.join(', ')}" if options[:verbose]
          # For databases that support #import (faster than #multi_insert)
          # SQLite does not appear to support #import at this time (April 2013) - SD
          #target_db[target_table].import(columns_union, source_dataset)
        end
      end
      return true
    end
    
    def self.map_row_to_renamed_columns(row, mapping)
      return row unless mapping&.any?
      new_row = {}
      row.each do |k,v|
        new_k = mapping[k] || k
        new_row[new_k] = v
      end
      new_row
    end
  end
end
