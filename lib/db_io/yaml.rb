require 'yaml'
require 'pathname'

module DB_IO

class YAML
  
  def marshall(file_path, object)
    File.open(file_path, "w") {|file| ::YAML.dump(object, file) }
  end 
  
  
  # Writes the table to a YAML file in the current directory, as an Array of Hashes, each Hash representing one row.
  # table is a Sequel::Dataset (sequel does not have an explicit Table class).
  def export(table_name, table, sort=true, options = {})
    defaults = {:convert_blobs_to_files => true}
    options = defaults.merge(options)
    
    file_name= "#{table_name}.yaml"
    
    # Grab entire table contents
    table_contents = table.export_rows
    table_contents = table_contents.sort_keys if sort
    
    # Parse any blobs out to files
    table_schema = table.db.schema(table_name)
    blob_cols = table.blob_columns
    if options[:convert_blobs_to_files] && blob_cols.any?
      # Make a directory based on the table name to store files in
      mkdir_p(table_name.to_s)
      table_contents.each_with_index do |row, index|
        blob_cols.each do |col, col_info|
          # Get the blob
          blob = row[col]
          # Skip the row if the blob is nil
          next unless blob
          
          # Write blob to file
          blob_path = Pathname.new File.join(table_name.to_s, "#{col}_#{index}")
          File.open(blob_path, 'w'){|bf| bf.puts blob }

          # Save placeholder in YAML (replacing blob content)
          row[col] = blob_path
        end # blob_cols.each
      end # table_contents.each
    end # blob_cols.any?
    
    # Write out table contents (possibly modified to replace blobs with pathnames) to file
    File.open(file_name, "w") {|file| ::YAML.dump(table_contents, file) } 
  end
  
  # Import file (written by companion #export) from current directory into specified table.
  # table is a Sequel::Dataset (sequel does not have an explicit Table class).
  def import(table_name, table, options = {}, target_db)
    options[:error_hash] ||= {}
    defaults = {:convert_blobs_to_files => true}
    options = defaults.merge(options)
    
    file_name= "#{table_name}.yaml"
    unless File.exist?(file_name)
      options[:error_hash][table_name]='Missing file'
      return options
    end
    begin
      rows = ::YAML.load_file(file_name)
      return if rows.empty?
      
      # Modify rows to re-insert blobs
      # Two options to identify blob columns. Rely on new schema correctly identifying blobs or check row contents.
      # Using schema: ... This isn't great because the new schema may not have the column marked as a blob (althought this would break things anyway)
      blob_cols = table.blob_columns
      # Using row contents: ...  This doesn't work since the first row may not have had a file
      # blob_cols = rows.first.select{|col, col_value| col_value.is_a?(Pathname) }
      if options[:convert_blobs_to_files] && blob_cols.any? && File.exist?(table_name.to_s)
        rows.each_with_index do |row, index|
          blob_cols.each do |col, col_info|
            # Get the pathname
            stored_pathname = row[col]
            # Skip the row if the path is nil
            next unless stored_pathname
          
            # Read the file to a blob
            # Two choices here, use stored pathname or rely on indices being unchanged
            # Using index:
            # blob_path = Pathname.new File.join(table_name.to_s, "#{col}_#{index}")
            # Using stored pathname:
            blob_path = stored_pathname
            
            next unless blob_path.exist?
            
            blob = File.binread(blob_path).to_sequel_blob

            # Save blob in YAML (replacing blob pathname)
            row[col] = blob
          end # blob_cols.each
        end # rows.each
      end # blob_cols.any?
      
      table.import_rows(rows, table_name, nil, options)
    rescue => err
      puts "Aborted YAML import for table: #{table_name}!"
      options[:error_hash][table_name]=err.message
    end
  end
end


end # module
