
module DB_IO

# Returns a Hash that identifies columns that specify RichText markup language.
# Answer is in the form:  { table_name => [col_1_name... col_N_name] }
def self.markup_language_columns(db)
    answer = Hash.new
    db.tables.each {|table_name|
        schema = db.schema(table_name)
        markup_columns = schema.select {|col_name, col_descriptor_hash|
            "integer" == col_descriptor_hash[:db_type] && col_name.to_s.index('_markup_language_id')
        }
        next if markup_columns.empty?
        col_names = markup_columns.collect {|col_name, col_descriptor_hash| col_name }
        answer[table_name]=col_names
    }
    answer
end

=begin
This sets the markup language for rich text.
language_name is one of the values in table gui_builder_profile_markup_types.
The current values are:
* Markdown
* LaTeX
* Plain
* HTML
* Textile
* Kramdown
=end
def self.markup_language_id(db, language_name)
    markup_type_table = db[:gui_builder_profile_markup_types]
    markup_type = markup_type_table[:value => language_name]
    raise "Failed to find markup language #{language_name.inspect}" unless markup_type
    markup_type[:id]
end

# column_spec_hash is in the form returned by markup_language_columns.
# If column_spec_hash is nil, the value from markup_language_columns is used
# language_name is a value suitable for markup_language_id
def self.set_markup_language(db, language_name, column_spec_hash=nil)
    column_spec_hash = markup_language_columns(db) unless column_spec_hash
    language_id = markup_language_id(db, language_name)
    column_spec_hash.each {|table_name, col_names|
        table = db[table_name]
        db.transaction {
            col_names.each {|col_name|
                # This updates all rows
                table.update(col_name => language_id)
            }
        }
    }
end

=begin
This grabs enumeration values from a src database that uses the old format (actual values),
and uses them to populate a dst database that uses the new format (references to another table).
The backup/restore functionality does not perform this kind of transformation.
src_url and dst_url can be created by DB_IO.setup
The mapping format is:
   { [src_table_name, dst_table_name] => {src_col_name => enum_table_name} }
It is presumed that
   * Rows of src_table map onto rows of dst_table: the two databases have the same structure (except for how enumerations are persisted)
   * The src_table has enumerations in the old format (actual values)
   * The dst_table has enumerations in the new format (references to another table)
   * The dst_table column name is derived from the src table column name by appending "_id"
=end
def self.migrate_enumerations(src_url, dst_url, mapping)
    src_db =  Sequel.connect(src_url) 
    dst_db =  Sequel.connect(dst_url) 
    # Don't comment out validation: the remaining code presumes the mapping is valid.
    validate_mapping(src_db, dst_db, mapping) 
    mapping.each {|table_names, col_info_hash|
        src_table_name, dst_table_name = table_names
        # table is a Sequel::Dataset (sequel does not have an explicit Table class).
        src_table = src_db[src_table_name]
        dst_table = dst_db[dst_table_name]
        dst_db.transaction {
            src_table.all.each {|src_row|
                row_id = src_row[:id]
                # This is a bit of a contortion.  Using Sequel without Models, rows are represented by Hashes.
                # Unlike Models, Hashes don't have a #save method. So to save a new value, we need to send
                # #update to dst_row_query (a Sequel::Dataset).  That affects every matching row.
                # #filter ensures that only one row is selected.
                dst_row_query = dst_table.filter(:id => row_id)
                raise "Bad import? Wrong number of rows (#{dst_row_query.count}) for id #{row_id.inspect} in destination table #{dst_table_name.inspect}." unless 1 == dst_row_query.count
                col_info_hash.each {|src_col_name, enum_table_name|
                    value = src_row[src_col_name]
                    next unless value
                    enum_table = dst_db[enum_table_name]
                    enum_row = enum_table[:value => value]
                    next puts("Failed to find enum value #{value.inspect} in enumeration table #{enum_table_name}") unless enum_row
                    enum_id = enum_row[:id]
                    dst_col_name = "#{src_col_name}_id".to_sym
                    dst_row_query.update(dst_col_name => enum_id)
                }
            }
        }
    }
end

# Verifies that all the required tables and columns are present.
# This prints errors as they are enountered, and then raises an exception at the end if there were any problems.
def self.validate_mapping(src_db, dst_db, mapping) 
    enum_tables = Array.new
    problematic_tables = Array.new
    mapping.each {|table_names, col_info_hash|
        src_table_name, dst_table_name = table_names
        # table is a Sequel::Dataset (sequel does not have an explicit Table class).
        src_table = src_db[src_table_name]
        dst_table = dst_db[dst_table_name]
        actual_src_cols = src_table ? src_table.columns : []
        actual_dst_cols = dst_table ? dst_table.columns : []
        required_src_cols = Array.new
        required_dst_cols = Array.new
        col_info_hash.each {|src_col_name, enum_table_name|
            enum_tables << enum_table_name
            required_src_cols << src_col_name
            required_dst_cols << "#{src_col_name}_id".to_sym
        }
        missing_src_cols = required_src_cols.uniq - actual_src_cols
        missing_dst_cols = required_dst_cols.uniq - actual_dst_cols
        problematic_tables << src_table_name if !src_table || !missing_src_cols.empty?
        problematic_tables << dst_table_name if !dst_table || !missing_dst_cols.empty?
        puts "Missing table #{src_table_name} in #{src_db.inspect}" unless src_table
        puts "Missing table #{dst_table_name} in #{dst_db.inspect}" unless dst_table
        puts "Missing source columns #{missing_src_cols.inspect} from #{src_db.inspect}" if src_table && !missing_src_cols.empty?
        puts "Missing destination columns #{missing_dst_cols.inspect} from #{dst_db.inspect}" if dst_table && !missing_dst_cols.empty?
    }
    enum_tables.uniq!
    enum_tables.each {|enum_table_name|
        next if dst_db[enum_table_name]
        puts "Missing enum table #{enum_table_name.inspect}"
        problematic_tables << enum_table_name
    }
    raise "Prombelms with tables: #{problematic_tables.inspect}" unless problematic_tables.empty?
end

end
