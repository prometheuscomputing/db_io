
class Object
  # Defaults to a no-op
  def fix_for_db_export; self; end
end

# require 'iconv'
class String
=begin
    Problems often appear to be caused by characters from Windows-1252 (also called CP-1252, often incorrectly identified as ISO-8259-1).
    The main tool for conversion is the C library "Iconv" (probably not available under Windows - may need Cygwin).
    Iconv can work with many encodings. You can get a list thusly (under Ruby 1.9):
        require 'pp'
        pp Iconv.list
    Some experiments:
        WINDOWS_TO_UTF8 = Iconv.new('UTF-8//TRANSLIT//IGNORE', 'Windows-1252')
        sample_utf8 = "ümlaut"
        puts sample_utf8.encoding.inspect                     # Strings default to UTF8.
        puts sample_utf8                                      # ümlaut        # As expected
        sample_utf8.inspect                                   # "\"ümlaut\""  # I don't understand why \" are shown. Ignore this for now.
        sample_w = sample_utf8.encode("cp1252")               # Now we have a sample non-UTF string
        puts sample_w.encoding.inspect                        # Says Windows-1252 even though we asked for "cp1252". Conclude Iconv treats "Windows-1252" and "CP-1252" identically.
        puts sample_w.inspect                                 # "\xFCmlaut"
        puts sample_w.clone.force_encoding("UTF-8").inspect   # "\xFCmlaut"  (Conclude that #inspect forces UTF-8 encoding)
        WINDOWS_TO_UTF8.iconv(sample_w).inspect               # "\"ümlaut\""
        sample_w.encode("UTF-8").inspect                      # "\"ümlaut\"" (Ruby probably use iconv under the covers. Even on Windows?)
        WINDOWS_TO_UTF8.iconv(sample_utf8).inspect            # "\"Ã¼mlaut\"" (#iconv presumes the "from" encoding is correct, it does not check the string's encoding). encode("UTF-8") is safer.
    BAD IDEA: attempt to fix individual Windows characters, as done in http://www.toao.net/48-replacing-smart-quotes-and-em-dashes-in-mysql).
        The reason it's a bad idea is that the entire string has a single encoding (individual characters are not differently encoded).
        So the entire string is either Windows-1252 or not.  If the entire string is Windows-1252, we really need to convert the entire string, not just the few characters specified at toao.net.
        This shows the mappings listed at toao.net.
        problems = {
          #  Problematic Windows-1252 character  =>    Similar looking UTF-8 character
          145.chr.force_encoding('Windows-1252') =>  [0xE2, 0x80, 0x98].pack('C*').force_encoding("UTF-8"), # "'"   Curly single open
          146.chr.force_encoding('Windows-1252') =>  [0xE2, 0x80, 0x99].pack('C*').force_encoding("UTF-8"), # "'"   Curly single close
          147.chr.force_encoding('Windows-1252') =>  [0xE2, 0x80, 0x9C].pack('C*').force_encoding("UTF-8"), # '"'   Curly double open
          148.chr.force_encoding('Windows-1252') =>  [0xE2, 0x80, 0x9D].pack('C*').force_encoding("UTF-8"), # '"'   Curly double close
          150.chr.force_encoding('Windows-1252') =>  [0xE2, 0x80, 0x93].pack('C*').force_encoding("UTF-8"), # '-'
          151.chr.force_encoding('Windows-1252') =>  [0xE2, 0x80, 0x94].pack('C*').force_encoding("UTF-8"), # '--'
          133.chr.force_encoding('Windows-1252') =>  [0xE2, 0x80, 0xA6].pack('C*').force_encoding("UTF-8")  # '...'
          }
          # These characters can all be converted by encode("UTF-8")
          puts problems.keys.join.encode("UTF-8")
          # Cannot detect garbage encoding of these characters (at least not by appending to a real UTF-8 string):
          problems.keys.join.force_encoding("UTF-8").each_char {|char| ' '+char }
    THE REAL PROBLEMS:  
        * We do not really know what the true encoding is. It can be a lie: Ruby (in Ramaze, give an string from a web broweser) or (given a string from an RDB) thinks/is-told it's UTF-8, but it's not.
        * If we guess the encoding incorrectly, the string is likely to contain "wrong-looking" characters. The incorrect guess *might* also result in "unreadable" characters. Wrong-looking is bad, but "unreadbale" throws errors.
    Some good references:
        * http://blog.grayproductions.net/categories/character_encodings
        * http://yehudakatz.com/2010/05/05/ruby-1-9-encodings-a-primer-and-the-solution-for-rails/
        * http://yehudakatz.com/2010/05/17/encodings-unabridged/
=end
=begin  # commented out for move to Ruby 2, which does not have iconv, and which does have scrub. I was not happy with this anyway.
  WINDOWS_TO_UTF8 = Iconv.new('UTF-8//TRANSLIT//IGNORE', 'Windows-1252')
  UTF8_FIXER = Iconv.new('UTF-8//IGNORE', 'UTF-8')  # 'UTF-8//TRANSLIT//IGNORE'
  def fix_UTF8
    answer = nil
    begin
      # Attempt to convert from whatever format Ruby *thinks* (based on input) the encoding is
      answer = self.encode("UTF-8")
      # Attempt to force examination of each character. It's not clear this works (See experiment in the "BAD IDEA" section of the comment)
      answer.each_char {|char| ' ' + char }
    rescue
      # Either the encoding was wrong, or the receiver is corrupted. Presume the encoding was wrong, it's actually Windows-1252
      # I'm using WINDOWS_TO_UTF8.iconv instead of force_encoding("UTF-8").encode("UTF-8") because I feel like I've got more control.
      answer = WINDOWS_TO_UTF8.iconv("#{self} ")[0..-2]
    end
    begin
      # Should be fine at this point.
      # Just to be certain, remove invalid UTF-8 characters, as explained at http://po-ru.com/diary/fixing-invalid-utf-8-in-ruby-revisited/
      answer = UTF8_FIXER.iconv("#{answer} ")[0..-2]
    rescue
      answer = "FAIED TO CONVERT STRING TO UTF-8"
    end
    answer
  end
=end
  def fix_UTF8; encode('UTF-8', :invalid => :replace, :undef => :replace); end
  # Should not do this for strings representing BLOBS.
  def fix_for_db_export; fix_UTF8; end
end

