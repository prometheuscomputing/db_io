require 'csv'

module DB_IO

class CSV
  
  attr_accessor :options
  
  def marshall(file_path, object)
    raise "DB_IO::CSV does not support marshall"
  end
  
  # Writes the table to a CSV file in the current directory.
  # table is a Sequel::Dataset (sequel does not have an explicit Table class).
  def export(table_name, table, sort=true)
    file_name= "#{table_name}.csv"
    columns = table.columns
    columns = columns.sort if sort
    ::CSV.open(file_name, 'w') {|csv| 
      csv <<  columns
      table.export_rows.each { |row| 
          csv << columns.collect { |col_name| row[col_name] }	
          }
      }
  end
  
  # Import file (written by companion #export) from current directory into specified table.
  # table is a Sequel::Dataset (sequel does not have an explicit Table class).
  def import(table_name, table, options={}, target_db)
    options[:error_hash] ||= {}
    file_name= "#{table_name}.csv"
    unless File.exist?(file_name)
      options[:error_hash][table_name]='Missing file'
      return options
    end
    begin
      row_arrays = ::CSV.read(file_name, options_for_table(table))
      header = row_arrays.shift # options configured to include headers as Array of Symbols
      importable_columns = header & table.columns
      index_lookup = Hash.new
      importable_columns.each {|col| index_lookup[col]=header.index(col) }
      rows = row_hashes(row_arrays, index_lookup)
      table.import_rows(rows, table_name, importable_columns, options)
    rescue => err
      options[:error_hash][table_name]=err.message
    end
  end
  
  # Converts rows from Array-form to Hash-form
  def row_hashes(row_arrays, index_lookup)
    row_arrays.collect {|row_array|
      row = Hash.new
      index_lookup.each {|col, index| row[col]=row_array[index] }
      row
      }
  end
  
  # Returns clone of options specialized to read a file corresponding to a given table
  def options_for_table(table)
    # need to set up converters, header_converters (header must return Symbols), etc
    # NOTE: fastercsv can transcode, using an option like :encoding=>'windows-1251:utf-8'
    raise "not yet implimented"
    {:headers => true, :header_converters => :symbol}
  end
    
end

end # module
