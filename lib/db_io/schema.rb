require 'pathname'

module DB_IO

class Schema

  def marshall(file_path, object)
    raise "DB_IO::Schema does not support marshall"
  end

  # Writes the schema for specified table to a YAML file in the current directory.
  # db is a Sequel::Database
  def export(table_name, table, sort=true)
    file_name= "#{table_name}.schema.yaml"
    schema = table.db.schema(table_name)
    schema = schema.sort_schema if sort
    File.open(file_name, "w") {|file| ::YAML.dump(schema, file) }
  end

  def import(table_name, table, options={}, target_db)

    #raise "Relational database schema is export only: import is not supported."
    file_name= "#{table_name}.schema.yaml"
    columns = ::YAML.load_file(file_name)
    primary_keys = columns.select{|c, i| i[:primary_key]}
    normal_columns = columns.reject{|c, i| i[:primary_key]}
    multiple_primary_keys = (primary_keys.count > 1)
    target_db.create_table(table_name) do
      if multiple_primary_keys
        primary_key(primary_keys.collect{|c, i| c})
      elsif primary_keys.any?
        primary_key(primary_keys.first[0])
      end
      normal_columns.each do |col, info|
        column(col, info[:type], :null => info[:allow_null])
      end
    end
  end
end

# TODO: $BACKUP_DIR may not be defined yet. Fix this.
# TODO: move this functionality into import? (not sure if this is a good idea/possible)
def self.create_db_from_schema(backup_dir = File.join($BACKUP_DIR, Date.today.to_s), db_type = :sqlite)
  case db_type
  when :sqlite
    sqlite_dir = File.join(backup_dir, 'SQLite')
    mkdir_p(sqlite_dir)
    db_file = File.join(sqlite_dir, 'SQL.db')
    if File.exist?(db_file)
      $BACKUP_DIR_REPLACE ? rm_rf(db_file) :  raise("Attempt to overwrite #{db_file.inspect}, not allowed by $BACKUP_DIR_REPLACE")
    end
    db = Sequel.sqlite(db_file)
  else
    raise "db_type #{db_type} is currently unsupported for creating a DB from a schema."
  end

  schema_dir = File.join(backup_dir, 'Schema') # TODO: do not hardcode Schema
  raise "Could not find Schema directory: #{schema_dir.inspect}" unless File.exist?(schema_dir)
  path = Pathname.new(schema_dir)
  path.children.each do |child_path|
    # Ignore subdirectories
    next if child_path.directory?
    # Ignore non-yaml
    next unless child_path.basename.to_s =~ /.schema.ya?ml/i
    table_name = child_path.basename.to_s.split('.schema.').first
    table_schema = ::YAML.load_file(child_path)
    primary_key_cols = table_schema.select{|col_name, col_info| col_info[:primary_key] }
    multiple_primary_keys = (primary_key_cols.length > 1)
    db.create_table table_name do
      table_schema.each do |col_name, col_info|
        # The options listed in col_info differ from those accepted by #column, so this is a mapping
        col_options = {
          :default => col_info[:default],
          :null => col_info[:allow_null]
          #:primary_key => col_info[:primary_key] # used elsewhere
          #:index => nil,
          #:unique => nil,
          #:foreign_key => ???,
          #:size => col_info[:column_size], # Not using since :size is not properly documented by Sequel, and I'm not sure it's needed.
          #:text => ???,
          #??? => col_info[:scale],
          #??? => col_info[:max_chars]
          #??? => col_info[:ruby_default]
          #??? => col_info[:db_type] # Unneeded, since this data is superceded by :type, but included here for reference
        }
        # Optionally convert to Sequel 'generic' column types.
        # For string, if :string is used as a type, then the column will return a Fixnum for numeric string content, so convert to String type
        col_type = case col_info[:type]
          when :string
            String
          when :integer
            Fixnum
          when :boolean
            TrueClass
          #when :datetime
          # etc.
          else
            col_info[:type]
          end

        if !multiple_primary_keys && col_info[:primary_key]
          column col_name, col_type, col_options
        else
          column col_name, col_type, col_options
        end

      end # iteration over schema columns

      if multiple_primary_keys
        primary_key primary_key_cols.collect{|col_name, col_info| col_name}
      end

    end # create table
  end # iteration over schema files
  $db = db
  db_file
end # self.create_db_from_schema


end # module
